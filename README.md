# mat1plot

## Description
This package is a collection of easy-to-use commands for plotting using SymPy. It is designed to fit the requirements of *01005 - Advanced Mathematics in Engineering 1* course at the Technical University of Denmark, found [here](https://01005.compute.dtu.dk/). This includes, but is not limited to:

- Plotting of vector fields
- 2D plotting of SymPy functions
- 3D plotting of SymPy functions

## Installation
To install mat1plot using PyPi, run the following command

``$ pip install mat1plot``

## Usage
Use is designed for the *01005 - Advanced Mathematics in Engineering 1* course at the Technical University of Denmark. Any usecase outside this scope is thus not considered, but very welcome!

## Contributing
You are very welcome to contribute in the way that makes sense to you! The development team will consider all pull requests, as well as mails directly to the developers ``s194345@student.dtu.dk`` or ``s194042@student.dtu.dk``.

## Authors and acknowledgment
The project would have never gotten off the ground without Jakob Lemvig and his engagement with SymPy in Mathematics 1. Thank you to Ulrik Engelund Pedersen for trusting us with this task. Finally, a huge thanks to the professors of the course for making great and engaging education: Micheal Pedersen and Karsten Schmidt.

## License
Open source under the BSD license. This means that you are free to use it whatever you like, be it academic, commercial, creating forks or derivatives, as long as you copy the BSD statement if you redistribute it (see the LICENSE file for details).

## Project status
This package is in pre-deployment phase. Simply setting it up for later deployment and use. Expected to be finished at end of 2022.
